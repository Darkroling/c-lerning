// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::Up;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElement.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		int32 ElementIndex = SnakeElement.Add(NewSnakeElement);
		if(ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);;
	float MovementSpeeds = ElementSize;
	switch (LastMoveDirection)
	{
	case EMovementDirection::Up:
		MovementVector.X -= MovementSpeeds;
		break;
	case EMovementDirection::Down:
		MovementVector.X += MovementSpeeds;
		break;
	case EMovementDirection::Left:
		MovementVector.Y -= MovementSpeeds;
		break;
	case EMovementDirection::Right:
		MovementVector.Y += MovementSpeeds;
		break;
	}

	//ddActorWorldOffset(MovementVector);

	for (int i = SnakeElement.Num() - 1; i > 0; i--)
	{
		ASnakeElementBase* CurrentElement = SnakeElement[i];
		ASnakeElementBase* PrevElement = SnakeElement[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElement[0]->AddActorWorldOffset(MovementVector);
}
