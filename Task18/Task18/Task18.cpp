﻿#include <iostream>
#include <string>
#include <algorithm> 
using namespace std;


class Player
{
public:
    string name;
    int score;

    void SetPlayer(const string& Player_Name, int Player_Score) {
        name = Player_Name;
        score = Player_Score;
    }
    bool operator<(const Player& other) const {
        return score > other.score;
    }
};

int main()
{
    int Number_Of_Player;
    cout << "Please, Enter number of player: " << endl;
    cin >> Number_Of_Player;

    Player* Players = new Player[Number_Of_Player];

    for (int i = 0; i < Number_Of_Player; ++i)
    {
        string name;
        int score;
        cout << "Enter name and score of " << i + 1 << " player: " << endl;
        cin >> name >> score;

        Players[i].SetPlayer(name, score);
   }
    sort(Players, Players + Number_Of_Player);

    cout << "Sort in descending order: " << endl;
    
    for (int i = 0; i < Number_Of_Player; ++i)
    {
        cout << i + 1 << ")  " << Players[i].name << "   " << Players[i].score << endl;
    }
    delete[] Players;
    return 0;
}

