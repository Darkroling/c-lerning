﻿#include <time.h>
#include <iostream>
struct tm buf;
time_t t = time(NULL);


using namespace std;

int main()
{
    int day = 0;
    localtime_s(&buf, &t);
    day = buf.tm_mday;
   
    const int N = 5;
    int remain = day % N;
    int array[N][N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++) {
            array[i][j] = i + j;
            cout << array[i][j] << "  ";
        }
        cout << endl;
    }

    cout << endl << endl;
    

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++) {
            if (i == remain)
            cout << array[i][j] << "  ";
        }
        cout << endl;
    }
    
}
