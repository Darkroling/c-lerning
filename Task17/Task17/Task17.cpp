﻿

#include <iostream>
#include <cmath>

class Vector
{
public:
	Vector() : x(), y(), z()
	{}
	Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
	{}


	void Print()
	{
		std::cout << '\n' << x << "   " << y << "   " << z;
	}

	void Modul()
	{
		double Modul = 0;
		Modul = sqrt(pow(x,2) + pow(y, 2) + pow(z, 2));
		std::cout << '\n' << "Vector's Module is " << Modul;
	}

private:
	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
	Vector Task_17(4, 7, 9);
	Task_17.Print();
	Task_17.Modul();
}

