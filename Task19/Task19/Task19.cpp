﻿#include <iostream>
using namespace std;


class Animal {
public:
	virtual void Voice() const = 0;
};

class Dog : public Animal {
public:
	void Voice() const override {
		cout << "Woof!\n";
	}
};

class Cat : public Animal {
public:
	void Voice() const override {
		cout << "Meow!\n";
	}
};

class Rat : public Animal {
public:
	void Voice() const override {
		cout << "Pii!\n";
	}
};

int main()
{
	Animal* Animals[3];
	Animals[0] = new Dog();
	Animals[1] = new Cat();
	Animals[2] = new Rat();

	for (Animal* a : Animals)
		a->Voice();
	return 0;
}
